import time
import sys
import Adafruit_DHT
from datetime import datetime
from flask import Flask, g, request, render_template
from w1thermsensor import W1ThermSensor
from datetime import date
import json
import sqlite3 as lite
from sqlite3 import Error


DS_SENSOR = W1ThermSensor()
DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4


app = Flask(__name__)

def insertNewLine(PathToDatabase, TableName, 
                    temp, humi, ulko, d):
    try:
        con = lite.connect(PathToDatabase, isolation_level=None)
        cur = con.cursor()  
        cur.execute("""
            INSERT INTO %s (sisa, sisaKosteus, ulko, datetime, device)
            VALUES(%s, %s, %s, datetime('now', 'localtime'), 'Raspberry Pi 3 Model B+')
            """ % (TableName, temp, humi, ulko))
        con.commit()
    except e:
        if con:
            con.rollback()
            print ("Error %s:" % e.args[0])
            sys.exit(1)
    finally:
        if con:
            con.close() 

@app.route('/')
def index():
    while True:
        humi, temp = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
        temperature = DS_SENSOR.get_temperature()
        d = date.today()
        print(d)
        insertNewLine("/home/pi/dhtdata.db", "marjastajanreitti", temp, humi, 0, d)
        time.sleep(1)
        return "%s Sis\xc3\xa4l\xc3\xa4mp\xc3\xb6tila on %s astetta. Ulkona on %s astetta. Ilmankosteus on %s" % (d, temp, temperature,humi)
        
@app.route('/tilasto')
def chart():
    index()
    con = lite.connect("/home/pi/dhtdata.db")
    con.row_factory = lite.Row
   
    cur = con.cursor()
    cur.execute("select * from marjastajanreitti")
       
    rows = cur.fetchall(); 
    data = []
    for r in rows:
        d = {'id': r[0], 'temp': r[1], 'humi': r[2], 'ulko': r[3],'datetime': r[4]}
        data.append(d)
    return render_template("chart.html",data = data)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
